##
##
## test class validated_envvar
##
## Author: Nathan Valentine <nrvale0@gmail.com>
##
##

import os

from validated_envvars import validated_envvars

class Test_validated_envvars:

    v = None

    def setup(self):
        self.v = validated_envvars()
        
    # FIXME: add tests for constructors with params
    def test___init__(self):
        assert isinstance(self.v,validated_envvars)

    def test__validate_w_success(self):
        os.environ['TESTKEY'] = 'TESTVALUE'
        assert self.v.validate( {'TESTKEY': '^TESTVALUE$'} )
        assert self.v.TESTKEY 

    def test_validate_w_failure(self):
        try:
            self.v.validate( {'TESTKEY': '^.+$'} )
        except SystemExit, e:
            pass
        finally:
            return True
        assert False
