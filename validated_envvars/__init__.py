##
##
## validated_envvar
##
## Author: Nathan Valentine <nrvale0@gmail.com>
##
##

import os
import re

class validated_envvars:

    def __init__(self, vars = None):
        pass

    def validate(self, envvars):
        for key, regex in envvars.iteritems():
            if None == re.match(regex, os.environ[key]):
                sys.exit("Validation of key \'{}\' failed!".format(key))
            else:
                setattr(self, key, os.environ[key])
        return True
    

if '__main__' == __name__:
    import sys
    import os

    if 2 != len(sys.argv):
        sys.exit("usage: {} <environment variable> <regex>".format(os.path.basename(sys.argv[0])))
    else:
        try:
            validated_envvars(sys.argv[0], sys.argv[1])
        except SystemExit, e:
            print "{} :: {}".format(e.errno, e.str)
            sys.exit(e.errno)
